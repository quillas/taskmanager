@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">{{ __('Menu') }}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <a href="{{ route('modifyTaskForm') }}" class="btn btn-success">Dodaj nowe zadanie</a>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Zadania') }}</div>
                <div class="card-body">
                    @if (count($tasksList) > 0)
                    @foreach ($tasksList as $task)
                    <div class="card mt-2">
                        <div class="card-body">
                            <h5 class="card-title mb-1">{{ $task['title'] }}</h5>
                            <p class="m-0">{{ __('Utworzono: ') }} {{ $task['created_at'] }}</p>
                            <p class="card-text mt-2">{!! $task['content'] !!}</p>
                            <form action="{{ route('deleteTask') }}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <input type="hidden" name="id" value="{{ $task['id'] }}" />
                                <button class="btn btn-danger float-right">Usuń</button>
                            </form>
                            <a href="{{ route('modifyTaskForm', $task['id']) }}" class="btn btn-success mr-2 float-right">Edytuj</a>
                        </div>
                    </div>
                    @endforeach
                    @else
                    {{ __('W tym momencie nie masz żadnych zadań.') }}
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
