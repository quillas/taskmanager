@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dodaj zadanie') }}</div>

                @if ($form_type === 'new')
                <div class="card-body">
                    <form action="{{ route('addTask') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="title">Tytuł</label>
                            <input type="text" name="title" class="form-control" 
                                   id="title" placeholder="{{ __('Wpisz tytuł zadania') }}" value="{{ $task['title'] }}">
                        </div>
                        <div class="form-group">
                            <label for="content">Treść</label>
                            <textarea id="editor" name="content" class="form-control" id="content" rows="4">{{ $task['content'] }}</textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Dodaj zadanie</button>
                    </form>
                </div>
                @endif

                @if ($form_type === 'edit')
                <div class="card-body">
                    <form action="{{ route('editTask') }}" method="POST">
                        {{ method_field('PUT') }}
                        @csrf
                        <input type="hidden" name="id" value="{{ $task['id'] }}">
                        <div class="form-group">
                            <label for="title">Tytuł</label>
                            <input type="text" name="title" class="form-control"
                                   id="title" placeholder="{{ __('Wpisz tytuł zadania') }}" value="{{ $task['title'] }}">
                        </div>
                        <div class="form-group">
                            <label for="content">Treść</label>
                            <textarea id="editor" name="content" class="form-control" id="content" rows="4">{{ $task['content'] }}</textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Edytuj zadanie</button>
                    </form>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-scripts')
        <script src="https://cdn.tiny.cloud/1/javcl9wfkqhq8de9cnlcgxqilc8ovrx6ww95ysztjybmqjh0/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
@endsection