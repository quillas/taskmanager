<?php

namespace App\Http\Controllers;

use \Illuminate\Contracts\Support\Renderable;
use App\Services\TaskManager;

class HomeController extends Controller
{

    /**
     * @var TaskManager
     */
    private $taskManager;

    /**
     * @return void
     */
    public function __construct(TaskManager $taskManager)
    {
        $this->middleware('auth');
        $this->taskManager = $taskManager;
    }

    /**
     * @return Renderable
     */
    public function index(): Renderable
    {
        $tasksList = $this->taskManager->getTasksByUserId(auth()->user()->id);

        return view('home', ['tasksList' => $tasksList]);
    }
}
