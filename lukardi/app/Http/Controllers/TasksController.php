<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\TaskManager;

class TasksController extends Controller
{

    /**
     * @var TaskManager
     */
    private $taskManager;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(TaskManager $taskManager)
    {
        $this->middleware('auth');
        $this->taskManager = $taskManager;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function modifyTaskForm(int $taskId = 0)
    {
        $userId = auth()->user()->id;
        $task = $this->taskManager->getSingleTask($taskId, $userId);

        return view('modify-tasks-form', [
            'form_type' => $task ? 'edit' : 'new',
            'task' => $task
        ]);
    }

    /**
     * @param Request $request
     * @return type
     */
    public function add(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|min:5',
            'content' => 'required|string|min:5'
        ]);
        $parameters = $request->all();
        $this->taskManager->createTask(
            $parameters['title'],
            $parameters['content'],
            auth()->user()->id
        );

        return redirect()->back()->with('success', __('Zadanie poprawnie dodane.'));
    }

    /**
     * @param Request $request
     * @return type
     */
    public function edit(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer',
            'title' => 'required|string|min:5',
            'content' => 'required|string|min:5'
        ]);
        $parameters = $request->all();
        $this->taskManager->editTask(
            $parameters['id'],
            $parameters['title'],
            $parameters['content'],
            auth()->user()->id
        );

        return redirect()->back()->with('success', __('Zadanie edytowano.'));
    }

    /**
     * @param Request $request
     * @return type
     */
    public function delete(Request $request)
    {
        $parameters = $request->all();
        $request->validate([
            'id' => 'required|integer'
        ]);
        $taskId = $parameters['id'];
        $this->taskManager->deleteTask($taskId, auth()->user()->id);

        return redirect()->back()->with('success', __('Zadanie poprawnie usunięte.'));
    }

}
