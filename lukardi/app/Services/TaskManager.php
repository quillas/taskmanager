<?php

namespace App\Services;

use App\Models\Task;
use Illuminate\Database\Eloquent\Collection;

class TaskManager
{

    /**
     * @var Task
     */
    private $task;

    /**
     * @param Task $task
     */
    public function __construct(Task $task)
    {
        $this->task = $task;
    }

    /**
     * @param string $title
     * @param string $content
     * @param int $userId
     * @return bool
     */
    public function createTask(string $title, string $content, int $userId): bool
    {
        $this->task['title'] = $title;
        $this->task['content'] = $content;
        $this->task['user_id'] = $userId;

        return $this->task->saveOrFail();
    }

    /**
     * @param int $taskId
     * @param string $title
     * @param string $content
     * @param int $userId
     * @return bool
     */
    public function editTask(int $taskId, string $title, string $content, int $userId): bool
    {
        $task = $this->getSingleTask($taskId, $userId);
        $task['title'] = $title;
        $task['content'] = $content;
        $task['user_id'] = $userId;

        return $task->saveOrFail();
    }

    /**
     * @param int $taskId
     * @param int $userId
     * @return Task|null
     */
    public function getSingleTask(int $taskId, int $userId): ?Task
    {
        return $this->task
                ->newQuery()
                ->select()
                ->from('tasks')
                ->where('user_id', $userId)
                ->where('id', $taskId)
                ->first();
    }

    /**
     * @param int $id
     * @return Collection
     */
    public function getTasksByUserId(int $id): Collection
    {
        return $this->task
                ->newQuery()
                ->select()
                ->from('tasks')
                ->where('user_id', $id)
                ->get();
    }

    /**
     * @param int $id
     * @param int $userId
     * @return bool
     */
    public function deleteTask(int $id, int $userId): bool
    {
        $task = $this->getSingleTask($id, $userId);

        if ($task) {
            return $task->delete();
        }
        throw new \InvalidParameterException('Nie można było usunąć zadadania.');
    }

}
