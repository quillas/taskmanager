<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/modifyTaskForm/{id?}', [App\Http\Controllers\TasksController::class, 'modifyTaskForm'])->name('modifyTaskForm');
Route::post('/addTask', [App\Http\Controllers\TasksController::class, 'add'])->name('addTask');
Route::put('/editTask', [App\Http\Controllers\TasksController::class, 'edit'])->name('editTask');
Route::delete('/deleteTask', [App\Http\Controllers\TasksController::class, 'delete'])->name('deleteTask');