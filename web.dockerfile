FROM nginx:1.10-alpine

ADD vhost.conf /etc/nginx/conf.d/default.conf
ADD nginx.conf /etc/nginx/nginx.conf

COPY public /var/www/public
