FROM php:7.3-fpm

ARG user
ARG uid

RUN apt-get update && apt-get -y install git && apt-get -y install zip && apt-get -y install nano

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN  apt-get install -y libmcrypt-dev \
        libmagickwand-dev --no-install-recommends \
        && pecl install mcrypt-1.0.2 \
        && docker-php-ext-install pdo_mysql \
        && docker-php-ext-enable mcrypt

RUN docker-php-ext-configure intl && docker-php-ext-install intl

RUN docker-php-ext-install mbstring exif pcntl bcmath gd
RUN useradd -G www-data,root -u $uid -d /home/$user $user
RUN mkdir -p /home/$user/.composer && \
    chown -R $user:$user /home/$user

WORKDIR /var/www

COPY database /var/www/database
COPY . /var/www

USER $user
